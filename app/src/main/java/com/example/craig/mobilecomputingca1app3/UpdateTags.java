package com.example.craig.mobilecomputingca1app3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class UpdateTags extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_tags);

        SQLiteDatabase db = new SQLiteDatabase(getApplicationContext());

        int dbnum = db.getNumOfStories();
        String dbtags = db.getTags();

        db.close();

        TextView name = (TextView)findViewById(R.id.textView2);

        name.setText("You current follow these tags: " +  dbtags);
    }

    public void AddTag(View v){

        SQLiteDatabase db = new SQLiteDatabase(getApplicationContext());

        int dbnum = db.getNumOfStories();
        String dbtags = db.getTags();

        boolean checktags = false;

        TextView name = (TextView)findViewById(R.id.editText);

        String addTag = name.getText().toString();

        String[] tags = dbtags.split(", ");

        for (int j = 0; j < tags.length; j++) {
            if (tags[j].equals(addTag)){
                checktags = true;
                Toast.makeText(getApplicationContext(),addTag + " is already in your tags!",Toast.LENGTH_SHORT).show();
            }
        }

        if(!checktags){
            dbtags = dbtags + addTag + ", ";
            Toast.makeText(getApplicationContext(),addTag + " has been added to your tags!",Toast.LENGTH_SHORT).show();
        }

        db.addPost(dbnum, dbtags);

        db.close();

    }

    public void RemoveTag(View v){

        String newTags = "";

        SQLiteDatabase db = new SQLiteDatabase(getApplicationContext());

        int dbnum = db.getNumOfStories();
        String dbtags = db.getTags();

        TextView name = (TextView)findViewById(R.id.editText2);

        String removeTag= name.getText().toString();

        String[] tags = dbtags.split(", ");

        for (int j = 0; j < tags.length; j++) {
            if (!tags[j].equals(removeTag)){
                newTags += tags[j]+ ", ";
            }
        }

        db.addPost(dbnum, newTags);

        db.close();

        Toast.makeText(getApplicationContext(),removeTag + " has been removed to your tags!",Toast.LENGTH_SHORT).show();

    }
}
