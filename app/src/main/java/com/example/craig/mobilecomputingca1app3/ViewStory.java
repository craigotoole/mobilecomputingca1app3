package com.example.craig.mobilecomputingca1app3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ViewStory extends AppCompatActivity{

    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_view_story);
        Bundle recdData = getIntent().getExtras();
        int myVal = recdData.getInt("position");
        MainActivity home = new MainActivity();

        TextView name = (TextView)findViewById(R.id.StoryName);
        TextView date = (TextView) findViewById((R.id.StoryDate));
        TextView author = (TextView)findViewById(R.id.StoryAuthor);
        TextView tags = (TextView)findViewById(R.id.StoryTags);
        TextView text = (TextView) findViewById((R.id.StoryText));

        name.setText(home.Names.get(myVal));
        date.setText(home.Dates.get(myVal));
        author.setText(home.Authors.get(myVal));
        tags.setText(home.Tags.get(myVal));
        text.setText(home.BodyOfText.get(myVal));

        id = home.ID.get(myVal);
    }
}
