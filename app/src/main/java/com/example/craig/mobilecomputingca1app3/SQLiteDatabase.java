package com.example.craig.mobilecomputingca1app3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Created by craig on 07/11/2017.
 */

public class SQLiteDatabase extends SQLiteOpenHelper {
    // Database Info
    private static final String DATABASE_NAME = "DataCache";
    private static final int DATABASE_VERSION = 1;

    Context ctx = null;

    // Table Names
    private static final String TABLE_POSTS = "user_data_cache";

    private static final String TAG = "MobileApp";
    private static final String KEY_POST_NUM_OF_STORIES = "numofstories";
    private static final String KEY_POST_USER_TAGS = "tags";

    public SQLiteDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        ctx = context;
    }

    @Override
    public void onCreate(android.database.sqlite.SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_POSTS +
                "(" +
                KEY_POST_NUM_OF_STORIES + " int," +
                KEY_POST_USER_TAGS + " TEXT" +
                ")";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(android.database.sqlite.SQLiteDatabase db, int i, int i1  ) {
        if (i != i1) {
            // Simplest implementation is to drop all old tables and recreate them
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_POSTS);
            onCreate(db);
        }
    }
    public void addPost(int numOfStories, String tags) {
        // Create and/or open the database for writing
        android.database.sqlite.SQLiteDatabase db = getWritableDatabase();

        // It's a good idea to wrap our insert in a transaction. This helps with performance and ensures
        // consistency of the database.
        db.beginTransaction();
        db.execSQL("delete from "+ TABLE_POSTS);
        try {

            ContentValues values = new ContentValues();
            values.put(KEY_POST_NUM_OF_STORIES, numOfStories);
            values.put(KEY_POST_USER_TAGS, tags);

            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            db.insertOrThrow(TABLE_POSTS, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }
    public String getTags(){
        String info = "";
        String POSTS_SELECT_QUERY =
                String.format("SELECT * FROM user_data_cache");

        android.database.sqlite.SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                info = cursor.getString(cursor.getColumnIndex(KEY_POST_USER_TAGS));
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return info;
    }
    public int getNumOfStories(){
        int info = 0;
        String POSTS_SELECT_QUERY =
                String.format("SELECT * FROM user_data_cache");

        android.database.sqlite.SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                info = cursor.getInt(cursor.getColumnIndex(KEY_POST_NUM_OF_STORIES));
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return info;
    }

}