package com.example.craig.mobilecomputingca1app3;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final List<String> Names = new ArrayList<String>();
    public static final List<String> Dates = new ArrayList<String>();
    public static final List<String> Authors = new ArrayList<String>();
    public static final List<String> BodyOfText = new ArrayList<String>();
    public static final List<String> Tags = new ArrayList<String>();
    public static final List<String> ID = new ArrayList<String>();

    Messenger myService = null;
    Messenger myServiceReceiver =  null;
    boolean isBound;

    CustomAdapter adapter = new CustomAdapter();
    ListView homePageList = null;
    Intent serviceIntent;

    public String retrievedStories = "";

    private ServiceConnection myConnection =
            new ServiceConnection() {
                public void onServiceConnected(ComponentName className,
                                               IBinder service) {
                    Log.d("MobileApp", "Conneting");

                    myService = new Messenger(service);
                    myServiceReceiver = new Messenger(new IncomingHandler());
                    isBound = true;

                    Message requestStories = Message.obtain(null, 0);
                    requestStories.replyTo = myServiceReceiver;
                    try{
                        myService.send(requestStories);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }

                public void onServiceDisconnected(ComponentName className) {
                    myService = null;
                    myServiceReceiver = null;
                    isBound = false;
                }
            };

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Bundle bundle = (Bundle) msg.obj;
                    retrievedStories = bundle.getString("key");

                    fillList();

                    homePageList.invalidateViews();

                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serviceIntent=new Intent();
        serviceIntent.setComponent(new ComponentName("com.example.craig.mobilecomputingca1app2","com.example.craig.mobilecomputingca1app2.RemoteBoundService"));

        homePageList = (ListView)findViewById(R.id.homePageList);
        homePageList.setClickable(true);

        homePageList.setAdapter(adapter);
        homePageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(getApplicationContext(), ViewStory.class);
                intent.putExtra("position", position);
                startActivity(intent);
            }

        });

    }

    @Override
    protected void onStart() {

        super.onStart();

        bindService(serviceIntent, myConnection, BIND_AUTO_CREATE);

        Log.d("MobileApp", "Binded");

    }

    @Override
    protected void onStop() {

        super.onStop();

        unbindService(myConnection);

        Log.d("MobileApp", "Not Binded");

    }

    class ViewHolder{
        TextView EventName;
        TextView EventDate;
        TextView EventAuthor;

        ViewHolder(View v){
            EventName = (TextView) v.findViewById(R.id.HomePageName);
            EventDate = (TextView) v.findViewById(R.id.HomePageDate);
            EventAuthor = (TextView) v.findViewById(R.id.HomePageAuthor);
        }
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return ID.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Log.d("row", "" +   i);

            ViewHolder holder = null;
            View row = view;
            if (view  == null){
                row = getLayoutInflater().inflate(R.layout.list_layout, null);
                holder =  new ViewHolder(row);
                row.setTag(holder);
            }else{
                holder = (ViewHolder) row.getTag();
            }

            holder.EventName.setText(Names.get(i));
            holder.EventDate.setText(Dates.get(i));
            holder.EventAuthor.setText(Authors.get(i));
            return row;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.tags_page:
                Intent intent1 = new Intent(this, UpdateTags.class);
                this.startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void fillList(){
        ID.clear();
        Names.clear();
        Dates.clear();
        Authors.clear();
        BodyOfText.clear();
        Tags.clear();

        SQLiteDatabase db = new SQLiteDatabase(getApplicationContext());

        int dbnum = db.getNumOfStories();
        String dbtags = db.getTags();

        try {

            JSONArray result = new JSONArray(retrievedStories);
            for (int i = 0; i < result.length(); i++) {

                JSONObject holder = (JSONObject) result.get(i);
                ID.add(holder.getString("id"));
                Names.add(holder.getString("name"));
                Dates.add(holder.getString("date"));
                Authors.add(holder.getString("author"));
                BodyOfText.add(holder.getString("bodyoftext"));
                Tags.add(holder.getString("tags"));

            }
            if(result.length() == dbnum) {

                Log.d("MobileApp", "Do nothing.");

            }else{

                db.addPost(result.length(), dbtags);

                Toast.makeText(getApplicationContext(),(result.length() - dbnum) + " new post has been added since you last checked!",Toast.LENGTH_SHORT).show();

                Log.d("MobileApp", "Update DB number of posts.");

            }
        } catch (JSONException e) {

        }

        db.close();

    }
}
